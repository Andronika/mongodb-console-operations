﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;

namespace MongoDbGettingStarted
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new MongoClient();
            var database = client.GetDatabase("MongoDatabase");
            var collection = database.GetCollection<BsonDocument>("Doc");

            var document = new BsonDocument
            {
                { "name", "MongoDB"},
                { "type", "Database"},
                { "count", 1},
                { "info", new BsonDocument
                    {
                        {"x", 203 },
                        {"y", 102 }
                    }}
            };

            //inserting
            collection.InsertOne(document);

            var documents = Enumerable.Range(0, 100).Select(i => new BsonDocument("counter", i));
            collection.InsertMany(documents);

            var count = collection.CountDocuments(new BsonDocument());

            //finding
            var firstDocument = collection.Find(new BsonDocument()).FirstOrDefault();
            
            var allDocuments = collection.Find(new BsonDocument()).ToList();

            //filtering, sorting
            var filterBuilder = Builders<BsonDocument>.Filter;
            var filterGtLt = filterBuilder.Gt("counter", 50) & filterBuilder.Lt("counter", 70);
            var docs = collection.Find(filterGtLt).ToList();
           
            var filter = Builders<BsonDocument>.Filter.Exists("counter");
            var sort = Builders<BsonDocument>.Sort.Descending("counter");

            var coll = collection.Find(filter).Sort(sort).First();

        }
    }
}
